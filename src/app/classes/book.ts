export class Book {
  
    public id:number ;
    public title:string;
    public author:string;
    public url:string;
  
    constructor (id: number,title: string, author: string,  url: string) {
      this.id = id;
      this.title = title;
      this.author = author;
      this.url = url;
    }

}