 import { Component } from '@angular/core';
 
 import { Book } from '../classes/book';
 
 @Component({
   selector: 'book-form',
   templateUrl: './book-form.component.html',
   styleUrls: ['./book-form.component.css']
 })
 export class BookFormComponent {
 
   model:Book;
 
   constructor() { 
     this.model = new Book(1, '', '', '');
   }
 
  get currentBook() { 
     return JSON.stringify(this.model); 
  }
 
 }