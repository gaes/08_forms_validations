##FORM##
https://medium.com/codingthesmartway-com-blog/angular-2-forms-tutorial-introduction-8686e0c2cd39

https://angular.io/guide/forms

##FORM##

ng-valid True if the form isvalid

ng-invalid True if the form is invalid

ng-pristine True if user has not interacted with the control yet.

ng-dirty True if user has already interacted with the control.

ng-touched  True if control has lost focus.

ng-untouched   True if control has not lost focus yet.



## REACTIVE FORM  and CUSTOM VALIDATIONS ##
https://stackoverflow.com/questions/39847862/min-max-validator-in-angular-2-final
https://angular.io/guide/reactive-forms
https://www.jokecamp.com/blog/angular-whitespace-validator-directive/#usage
